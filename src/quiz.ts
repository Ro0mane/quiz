
const pictures = ['Pisteverte.png', 'Pisteverte.png', 'Pisteverte.png', 'Pistebleu.png', 'Pistebleu.png', 'Pistebleu.png', 'Pisterouge.png', 'Pisterouge.png', 'Pisterouge.png', 'Pistenoire.png', 'Pistenoire.png', 'Pistenoire.png', 'Horspistes.png']
const p1textQuestion: string[] = ["Avant de partir glisser sur les pistes on mets:", "Comment frainent les débutants ?", "Quel est la médaille la plus basse (pour les débutants) de l'ESF ?", "Quand faut-il freiner lors d'un virrage ?", "Quel est le nom de l'engin de remonté machanique qui nous tire par les fesses ?", "Il est recomandé de :", "Comment appel-t-on la technique ou l'ont fait des petits virages les skis sérés ?", "Quel est le nom du lit attaché à l'arrière des motos neige des secouristes ?", "Que signifit des panneaux ou des piquets jaune et noir ?", "Quel est le nom des skis qui permettent de faire des figures et du hors piste ?", "A qui est la prioritée sur les pistes ?", "Comment appelle-t-on les bords sur les côté des skis ?", "Dans quelle station a été tourné le film Les Bronzés font du ski"]
const p1reponseCorrect: string[] = ["Des skis", "En chasse neige ", "Le piou piou", "Avant", "Un téléski", "Porter un casque", "La godille", "Une barquette", "Un danger", "Ski freestyle", "A celui qui est le plus bas", "Les carres", "Val D'Isère"]
const p1reponse1: string[] = ["Des rollers", "En chasse d'eau", "Le cui cui", "Après", "Un télésiège", "Skier nu", "La goldric", "Un traineau", "Il y a une ruche pas loin", "Ski de randonnée", "Au plus jeune", "Les fourchettes", "La Plagne"]
const p1reponse2: string[] = ["Des baguettes", "En canard", "Le poussin", "Pendant", "Un téléfil", "Skier tout droit", "La chenille", "Un brancard", "C'est un slalom", "Ski de fond", "A celui qui vient d'en haut", "Les coins", "Chamonix"]


const sectImages = document.getElementById('img')
const options = document.getElementById('options')
const buttons = document.getElementsByClassName('bouton')
const h2textquestion = document.getElementById('textQuestion')
const btn1 = document.createElement('button')
const btn2 = document.createElement('button')
const btn3 = document.createElement('button')
const scoreTotal = document.getElementById('score')

const nextButton = document.getElementById('nextB')
const h3res = document.getElementById('res')


const proba: number[] = []



console.log(proba);


let listreponses = [btn1, btn2, btn3]
let scoreN = 0;
let numQ = 0;
scoreTotal.innerHTML = scoreN + '/13';

if (sectImages && h2textquestion && btn1 && btn2 && btn3) {
    sectImages.setAttribute('src', pictures[numQ])
    h2textquestion.innerHTML = p1textQuestion[numQ];
    creeButtons()

}

let rp = false


for (let i = 0; i < buttons.length; i++) {
    buttons[i]?.addEventListener('click', () => {
        if (!rp) {

            if (buttons[i] == listreponses[proba[0]]) {
                listreponses[proba[0]].style.backgroundColor = "#2ecc71";
                h3res.innerHTML = 'Correct !';
                scoreN++

                nextButton.style.display = "flex"
                rp = true

            } else {
                listreponses[proba[1]].style.backgroundColor = "red";
                listreponses[proba[2]].style.backgroundColor = "red";

                h3res.innerHTML = 'Incorrect. La réponse correcte est ' + p1reponseCorrect[numQ];

                nextButton.style.display = "flex"
                rp = true

            }
            scoreTotal.innerHTML = scoreN + '/13';

        }

    });
}


nextButton.addEventListener('click', () => {
    nextButton.style.display = "none"
    h3res.innerHTML = '';

    numQ++

    if (p1textQuestion[numQ]) {
        btn3.style.backgroundColor = "cadetblue";
        btn1.style.backgroundColor = "cadetblue";
        btn2.style.backgroundColor = "cadetblue";

        sectImages.setAttribute('src', pictures[numQ])
        h2textquestion.innerHTML = p1textQuestion[numQ];
        creeButtons()
    } else {
        showPopup(scoreN)
    }

    rp = false


})



function creeButtons() {
    options.innerHTML = ''

    btn1.classList.add('bouton')
    btn2.classList.add('bouton')
    btn3.classList.add('bouton')

    options.append(btn1)
    options.append(btn2)
    options.append(btn3)

    proba[0] = Math.round(Math.random() * 2)
    prob1()
    prob2()

    function prob1() {
        proba[1] = Math.round(Math.random() * 2)
        if (proba[1] == proba[0]) {
            prob1()
        }
    }

    function prob2() {
        proba[2] = Math.round(Math.random() * 2)
        if (proba[2] == proba[0] || proba[2] == proba[1]) {
            prob2()
        }
    }

    listreponses[proba[0]].innerHTML = p1reponseCorrect[numQ];
    listreponses[proba[1]].innerHTML = p1reponse1[numQ];
    listreponses[proba[2]].innerHTML = p1reponse2[numQ];
}

function showPopup(score: number) {
    const finalMessage: string = `Félicitations ! Votre score final est de ${score}/13. Merci d'avoir joué !`;

    const popupElement: HTMLDivElement = document.createElement('div');
    popupElement.classList.add('popup');

    const messageElement: HTMLParagraphElement = document.createElement('p');
    messageElement.textContent = finalMessage;

    const restartButton: HTMLButtonElement = document.createElement('button');
    restartButton.textContent = 'Recommencer';
    restartButton.addEventListener('click', () => {
        location.reload();
    });

    popupElement.appendChild(messageElement);
    popupElement.appendChild(restartButton);

    document.body.innerHTML = '';
    document.body.appendChild(popupElement);
}
