
import { resolve } from 'path'
import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    rollupOptions: {
      input: {
        quiz: resolve(__dirname, 'quiz.html'),
      },
    },
  },
})
