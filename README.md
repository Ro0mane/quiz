J'ai choisi de faire un quiz avec plusieurs niveaux de difficulté représentés par les panneaux des pistes de ski.
Il y a donc 4 niveaux de 3 questions et une dernière question en "Hors piste".
Je me suis servis d'HTML, CSS, TypeScript et des flexbox.
A la fin du quiz j'ai mis un bouton pour pouvoir le recommencer.

Thème : Ski

"Le saviez vous ?" pour certaines questions

3 questions par partie

    Partie 1 (piste verte)

- Comment appel-ton ces objets ? (photo de ski)
- Des rasoires
- Des skis X
- Des baguettes

- Comment frainent les débutants ?
- En canard
- En chasse d'eau
- En chasse neige X

- Quel est la médaille la plus basse (pour les débutants) de l'ESF ?
- Le cui cui
- Le piou piou X
- Le poussin

  Partie 2 (Piste bleu)

- Quand faut-il freiner lors d'un virrage ?
- Avant X
- Pendant
- Après

- Quel est le nom de cet engin ? (photo de tire fesses) (On l'appel aussi remonte pente ou tire-fesse)
- Un télésiège
- Un téléfil
- Un téléski X

- Il est recomandé de : (Malgré de nombreux accidents, le casque n'est pas obligatoire dans la loi, cependant les écoles de ski l'obligent très souvent)
- Skier nu
- Porter un casque X
- Skier tout droit

  Partie 3 (Piste rouge)

- Comment appel-t-on la technique ou l'ont fait plein de petits virages à la suite les skis sérés ? (Le saviez vous ? Il faut obligatoirement savoir faire la godille pour avoir sa 3ème étoile, c'est une technique un peu dépassée car le matériel de nos jours permet plus de fluidité)

  - La godille X
  - La chenille
  - La goldric

- Quel est le nom du lit attaché à l'arrière des motos neige des secouristes ? (Photo d'une barquette)

  - Un traineau
  - Un brancard
  - Une barquette X

- Que signifit des panneaux ou des piquets jaune et noir ?

  - Il y a une ruche pas loin
  - Un danger X
  - C'est un slalom

  Partie 4 (Piste noir)

- Quel est le nom de ces skis ? (photo de ski Freestyle) (Les skis freestyle sont aussi plus long que la normal, permettant une meilleure flottabilité beaucoup mieux si l'on souhaite s'aventurer dans la poudreuse en hors-piste et réaliser des figures)
- Ski de randonnée
- Ski freestyle X
- Ski de fond

- A qui est la prioritée sur les pistes ? (La priorité est toujours à ceux qui sont le plus bas, on dis qu'ils sont en aval.)
- Au plus jeune
- A celui qui vient d'en haut
- A celui qui est le plus bas X

- Comment appelle-t-on les bords sur les côté des skis ?
- Les fourchettes
- Les carres X
- Les coins

  Partie 5 (hors piste, question bonus) 1 question

- Dans quelle station est tourné le film "Les Bronzés font du ski" ?
- Chamonix
- Val D'Isère X
- La Plagne
